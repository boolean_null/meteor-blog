
AccountsTemplates.addFields([
    {
        _id: "username",
        type: "text",
        required: true
    }
]);

AccountsTemplates.configure({
    // confirmPassword: false,

    onSubmitHook: function(error, state){
        if (state === "signIn") {
           if (Meteor.userId()) {
               $('#login-modal').modal('hide');
           }
        }
    },
    onLogoutHook: function(){
        alert('logged out');
        
        FlowRouter.go('/');
    },

    texts: {
        button: {
            signUp: "Register Now!"
        },
        socialSignUp: "Register",
        socialIcons: {
            "meteor-developer": "fa fa-rocket"
        },
        title: {
            forgotPwd: "Recover Your Password"
        },
    },
});