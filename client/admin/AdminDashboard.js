
Template.AdminDashboard.onCreated(function () {
    this.autorun(() => {
        this.subscribe('categories');
        this.subscribe('users');
        this.subscribe('blogposts');
        
    });
});

Template.AdminDashboard.helpers({

    'categories': () => Categories.find({}, {
        sort: {count: -1}
    }),
    'users': () => Meteor.users.find(),

    'userPosts': (userId) => BlogPosts.find({createdBy: userId}).fetch().length

});



Template.AdminDashboard.events({
    'submit form': (e)=>{
        e.preventDefault();


        var cat = $('#input-category').val();

        // no special character and 
        // spaces only regex
        if (!/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(cat) && cat.replace(/\s/g, '').length) {
            alert('success');
            Meteor.call('createCategory', cat)
        }else{
            alert('nope');
        }
    }
});

