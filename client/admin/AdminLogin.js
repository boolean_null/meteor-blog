
Template.AdminLogin.events({
    'submit form': (e, template) => {
        e.preventDefault();
        var username = template.find('#admin-username').value;
        var password = template.find('#admin-password').value;

        Meteor.loginWithPassword(username, password, function (invalid) {
            
            if (invalid) {
                alert('invalid credentials');
                
                /* template.find('#login-inputEmail').value = null;
                template.find('#login-inputPassword').value = null; */
            }else{
                window.location.href = "/admin/";
            }
        });
    }
});


Template.AdminLogin.helpers({
    
});