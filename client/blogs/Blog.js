/* Session.set('postId', FlowRouter.getParam('id'));
postId = Session.get('postId');

id = FlowRouter.getParam('id'); */

// Meteor.subscribe('blogpost');

/* Template.Blog.onCreated(()=>{
    var self = this;
    self.autorun(()=>{
        self.subscribe('blogpost');
    });
});
 */

 Template.Blog.onCreated(function () {
    this.autorun(() => {
        var id = FlowRouter.getParam('id');
        
        this.subscribe('singleBlogPost', id);
    });
});

Template.registerHelper('or', function (a, b) {
    return true;
});

Template.Blog.helpers({
    blog: () => BlogPosts.findOne({_id: FlowRouter.getParam('id')}),
    
    isCreator:()=>Meteor.userId() == BlogPosts.findOne({ _id: FlowRouter.getParam('id') }).createdBy ? true : false,

    isCommentor:(userEmail)=>{
        var email = Meteor.users.find().fetch()[0].emails[0].address;

        if (email == userEmail) 
            return true;
        else
            return false;
        
    }
});


Template.Blog.events({
    'submit form': (e) => {
        e.preventDefault();

        if (Meteor.userId()) {
            var comment = $('#input-comment').val();
            var userId = Meteor.users.find().fetch()[0].emails[0].address;
            var date = '01/01/01';
            var postId = FlowRouter.getParam('id');

            // alert(userId);

            Meteor.call('createComment', comment, userId, date, postId);
            showMessage('comment posted', 'success');

        }else{
            $('#login-modal').modal('show');
        }
    },
    'click .delete-post' : () => {
        var postId = FlowRouter.getParam('id');
        if (confirm("Are you sure?") == true) {
            Meteor.call('deleteBlogPost', postId);
        } else {
        }
    },

    'click .save-edit':()=>{
        Session.set('editing', null);

        Meteor.call('editCommentSave');

    }

});









