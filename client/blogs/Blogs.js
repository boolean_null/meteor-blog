// Meteor.subscribe('blogposts');


Template.Blogs.onCreated(function () {
    this.autorun(() => {
        this.subscribe('blogposts');
    });
});



Template.Blogs.helpers({
    blogposts: () => {
        return BlogPosts.find();
    }
});