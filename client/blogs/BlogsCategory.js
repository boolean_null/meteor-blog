
Template.BlogsCategory.onCreated(function () {
    this.autorun(() => {
        var cat = FlowRouter.getParam('category');
     
        this.subscribe('blogsPerCat', cat);
        // this.subscribe('singleCat', cat);
        
    });
});


Template.BlogsCategory.helpers({

    blogs: () => BlogPosts.find(),
    comments: blogId => BlogPosts.findOne({ _id: blogId }).comments.length,
    creator: () => Meteor.users.find().fetch()[0].emails[0].address,
    cat: () => FlowRouter.getParam('category')

});

Template.BlogsCategory.events({
    'click .btn-discussion': ()=>{
        if (Meteor.userId()) {
            var cat = FlowRouter.getParam('category');
            FlowRouter.go('/blogs/'+cat+'/discussion');           
        }else{

            $('#login-modal').modal({
                show: 'true'
            }); 
        }
    }
});