

Template.CatDiscussion.onCreated(function () {
    this.autorun(() => {
        var cat = FlowRouter.getParam('category');

        this.subscribe('singleCat', cat);
    });
});


Template.CatDiscussion.helpers({

    getCategory: () => FlowRouter.getParam('category'),
    category: () => Categories.findOne()
}),


Template.CatDiscussion.events({
    'submit form': (e)=>{
        e.preventDefault();
        
        var catId = Categories.findOne()._id
        var name = Meteor.user().username;
        var message = $('#input-message').val();
        var time = new Date();
        
        
        //if no  whitespace
        if (message.replace(/\s/g, '').length){
            Meteor.call('sendChat', catId, name, message, time);
            $('#input-message').val('');
        }
    }
})