
Template.CreateBlog.onCreated(function () {
    this.autorun(() => {
        this.subscribe('categories');
    });
});




Template.CreateBlog.helpers({
    categories: () =>{
        return Categories.find();
    },
});

Template.CreateBlog.events({

    'submit form': (e) => {
        e.preventDefault();
        var cats = [];

        var title  = $('#input-blogTitle').val();
        var body = $('#input-blogBody').val();

        $.each($("input[name='category']:checked"), function () {
            cats.push($(this).val());
        });


        Meteor.call('updateCategory', cats);
        Meteor.call('createBlogPost', title, body, cats);
    }

});
