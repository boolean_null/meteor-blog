Template.EditBlog.onCreated(function () {
    this.subscribe('categories');
    this.subscribe('singleBlogPost', FlowRouter.getParam('id'));
});


Template.EditBlog.helpers({
    categories: () => Categories.find(),

    blog: () => BlogPosts.findOne({ _id: FlowRouter.getParam('id') }),

});


Template.EditBlog.events({
    'submit form': (e)=>{
        e.preventDefault();

        var categories = BlogPosts.findOne({ _id: FlowRouter.getParam('id') }).categories;
       

     /*    $("input[name='category']").each(function () {
            
            
            if (this.value == "Programming") {
                var test = 'Programming';
                $("input[type='checkbox']").prop('checked', true);
                $("input[value=test]").prop('checked', true);
            }
        }); */

        var cats = [];

        var title = $('#input-blogTitle').val();
        var body = $('#input-blogBody').val();

        $.each($("input[name='category']:checked"), function () {
            cats.push($(this).val());
        });

        var postId = FlowRouter.getParam('id')
        Meteor.call('updateBlogPost', postId, title, body, cats);
        
        // Meteor.call('createBlogPost', title, body, cats);
    }
})

// $('.myCheckbox').prop('checked', true);