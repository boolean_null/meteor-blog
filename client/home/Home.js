

Template.Home.onCreated(function () {
    this.autorun(() => {
        this.subscribe('categories');
    });
});


Template.Home.helpers({
    categories: () => {
        return Categories.find({}, {
            sort: {count: -1}
        });
    }
});


