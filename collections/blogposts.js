BlogPosts = new Mongo.Collection('blogposts');

Meteor.methods({
    'createBlogPost': (title, body, cat) => {
        check(title, String);
        check(body, String);
        check(cat, Array);

        BlogPosts.insert({
            title: title,
            body: body,
            createdBy: Meteor.userId(),
            createdAt: '01/01/01',
            categories: cat,
            comments: [{
                userId: '',
                comment: '',
                date: ''
            }]
        });

        console.log('blog inserted');
    },
    'createComment': (comment, userId, date, postId) => {
        BlogPosts.update({
            _id: postId
        }, {
                $push: {
                    "comments": {
                        "commentId": Random.id(),
                        "userId": userId,
                        "comment": comment,
                        "date": new Date().toLocaleDateString()
                        
                    }
                }
            });
    },
    'deleteBlogPost': (postId) => {
        BlogPosts.remove({_id: postId});
        console.log('post deleted');
    },

    'updateBlogPost': (postId, title, body, cats) =>{
        check(title, String);
        check(body, String);
        check(cats, Array);


        BlogPosts.update({ _id: postId }, { 
            $set: { 
                title: title,
                body: body,
                categories: cats
            } 
        });

        console.log('blog updated');
    },

    'testFunction': (id)=>{
        alert(id);
    },

    'deleteComment': (postId, commentId)=>{
        BlogPosts.update({ _id: postId }, {
            $pull: { comments: { commentId: commentId } }
        });
    },
    'editComment':(postId,commentId)=>{
        if (Meteor.isClient) {
            $('div #'+commentId).attr('contenteditable', 'true');
            Session.set('editing', commentId);
        }
    },
    'editCommentSave':()=>{
        /* BlogPosts.update( {_id: 'wZLEm3WAWLjQNj5Rp'}, {$set: {
            comments: { commentId: commentId }
        }});

        BlogPosts.update({ _id: 'wZLEm3WAWLjQNj5Rp' }, {
            $set: {
                comments: { commentId: "KXjWuta3wdyJLaEBx" }
            }
        });

        { $set: { comments : "anything" } } } */
       /*  BlogPosts.update({ _id: "wZLEm3WAWLjQNj5Rp" }, {
            $set: { comments: [{ commentId: "yb9SAARGDDbHhTCTr" }] } }); */
/* 
        BlogPosts.update(
            { _id: "wZLEm3WAWLjQNj5Rp", "comments.commentId": "yb9SAARGDDbHhTCTr" },
            { $set: { "comments.comment": "newData" } }
        );
 */
        /* BlogPosts.update(
            { _id: 'z2wEcBM5sHbFrCrdb', 'comments.commentId': '1' },
            {
                '$set':
                {
                    'comments': [{
                        'comment': "newComment"
                    }]
                }
            }
        ) */

        // alert('asdlfkajsdflkj');
        BlogPosts.update(
            { _id: 'z2wEcBM5sHbFrCrdb', 'comments.commentId': 'wuQ58HvYbRRj9PsKH' },
            {
                '$set':
                {
                    'comments': {
                        '$.comment': "newComment"
                    }
                }
            }
        )

        /* db.collection.update(
            { 'userId': 'foL9NpoZFq9AYmXyj', 'comments.commentId': 'yb9SAARGDDbHhTCTr' },
            {
                '$set':
                {
                    'comments': [{
                        'comment': {
                            'members': {
                                'squad1': [{ 'username': 'John', 'death': 1 }]
                            }
                        }
                    }]
                }
            }
        ) */

       /*  BlogPosts.update({ _id: postId }, {
            $pull: { comments: { commentId: commentId } }
        }); */
        
   
    }

});




/* "_id": "3AyLHQnRyDiswcg72",
  "title": "title",
  "body": "body",
  "createdBy": "user1",
  "createdAt": "01/01/01",
  "categories": [
    "cat1",
    "cat2",
    "cat2"
  ],
  "comments": [
    {
      "commentId": "1",
      "userId": "",
      "comment": "",
      "date": ""
    },
    {
      "commentId": "2",
      "userId": "dens@dens",
      "comment": "test",
      "date": "01/01/01"
    },
    {
      "commentId": "3",
      "userId": "dens@dens",
      "comment": "test\n",
      "date": "01/01/01"
    },
    {
      "commentId": "4",
      "userId": "admin@admin",
      "comment": "by admin",
      "date": "01/01/01"
    }
  ] */