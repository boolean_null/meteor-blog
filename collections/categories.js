Categories = new Mongo.Collection('categories');


Meteor.methods({
    // createCategory: (cat) => {
    'createCategory': function (cat) {
        check(cat, String);

        Categories.insert({
            name: cat,
            count: 0,
            messages: []
        });
        console.log('category inserted');

        
    },

    'updateCategory': (cats) =>{
        console.log(cats);
        for(cat in cats){
            console.log(cats[cat]);

            Categories.update({name: cats[cat]}, {
                $inc: {
                    count: 1
                }
            });
        }
    },

    'sendChat': (catId, name, message, time)=>{
        Categories.update({ '_id': catId }, {
            $push: {
                messages: {
                    messageId: Random.id(),
                    name: name,
                    message: message,
                    time: time
                }
            }
        });
    }
});