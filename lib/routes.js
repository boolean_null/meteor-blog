FlowRouter.route('/', {
    name: 'home',
    action(){
        BlazeLayout.render('MainLayout', { main: 'Home'});
    }
});

FlowRouter.route('/blog/create', {
    name: 'create',
    action(){
        BlazeLayout.render('MainLayout', { main:'CreateBlog'});
    }
});


FlowRouter.route('/blogs', {
    name: 'blogs',
    action(){
        BlazeLayout.render('MainLayout', {main: 'Blogs'});
    }
});

FlowRouter.route('/blogs/:category', {
    name: 'blogs_category',
    action(){
        BlazeLayout.render('MainLayout', { main: 'BlogsCategory'})
    }
});

FlowRouter.route('/blogs/:category/discussion', {
    name: 'blogs_category',
    action() {
        BlazeLayout.render('MainLayout', { main: 'CatDiscussion' })
    }
});



FlowRouter.route('/blog/:id', {
    name: 'blog',
    action() {
        BlazeLayout.render('MainLayout', { main: 'Blog' });
    }
});

FlowRouter.route('/blog/edit/:id', {
    name: 'blog_edit',
    action() {
        BlazeLayout.render('MainLayout', { main: 'EditBlog' });
    }
});

// User Routes

FlowRouter.route('/profile/:id', {
    name: 'user',
    action() {
        BlazeLayout.render('MainLayout', { main: 'UserLayout' });
        
        /* var role_admin = Roles.userIsInRole(Meteor.userId(), 'admin');

        // alert(role_admin);

        if (Meteor.userId()) {
            if (role_admin) {
                BlazeLayout.render('MainLayout', { main: 'AdminDashboard' });
            } else if(!role_admin) {
                BlazeLayout.render('MainLayout', { main: 'UserDashboard' });
            }
        } */
    }
});

/* FlowRouter.route('/profile/:id/posts', {
    name: 'user_posts',
    action() {
        alert('posts');
       
    }
}); */

// admin routes

FlowRouter.route('/admin', {
    name: 'admin',
    action(){
        BlazeLayout.render('AdminLayout', {main: 'AdminDashboard'});
    }
})


FlowRouter.route('/admin/login', {
    name: 'admin_login',
    action(){
        BlazeLayout.render('AdminLayout', {main: 'AdminLogin'});
    }
});