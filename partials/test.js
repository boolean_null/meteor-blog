showMessage = (message, type) => {
    $.notify({
        message: message
    }, {
        type: type,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}


