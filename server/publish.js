
// BLOGS 
Meteor.publish('blogposts', () => BlogPosts.find());
Meteor.publish('blogsPerUser', userId =>  BlogPosts.find({createdBy: userId}));
Meteor.publish('singleBlogPost', id =>  BlogPosts.find({ _id: id }));
    



// CATEGORIES
Meteor.publish('categories', () =>  Categories.find());
Meteor.publish('blogsPerCat', cat => BlogPosts.find({ categories: cat }));
Meteor.publish('singleCat', cat => Categories.find({ name: cat }));


// USERS
Meteor.publish('users', () => Meteor.users.find());



   

